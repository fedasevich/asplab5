using Microsoft.Extensions.Logging;
using WebApplication5;


var builder = WebApplication.CreateBuilder(args);


builder.Logging.AddProvider(new FileLoggerProvider(Path.Combine(Directory.GetCurrentDirectory(), "logger.txt")));

var app = builder.Build();
app.Use(async (context, next) =>
{
    try
    {
        await next();
    }
    catch (Exception exeption)
    {
        app.Logger.LogError(exeption, $"An error occurred while processing the request: {exeption.Message}");
        await context.Response.WriteAsync($"Error happened: {exeption.Message}");
    }
});

app.MapGet("/", async (context) =>
{

    var valueFromCookies = context.Request.Cookies["StoredValue"];

    await context.Response.WriteAsync($"Value stored in Cookies: {valueFromCookies}");
});

app.MapGet("/error", async (context) =>
{
    int a = 5;
    int b = 0;
    int c = a / b;
    await context.Response.WriteAsync($"c = {c}");

});

app.MapGet("/form", async (context) =>
{
    var response = context.Response;
    response.ContentType = "text/html; charset=utf-8";

    await response.WriteAsync(@"
                <form method=""post"" action=""/form"">
                    <label for=""value"">Value:</label>
                    <input type=""text"" name=""value"" id=""value"" /><br />
                    <label for=""datetime"">Date and time:</label>
                    <input type=""datetime-local"" name=""datetime"" id=""datetime"" /><br />
                    <input type=""submit"" value=""Save in Cookies"" />
                </form>
            ");
});

app.MapPost("/form", (HttpContext context) =>
{

    var value = context.Request.Form["value"];
    var datetime = context.Request.Form["datetime"];

    DateTime inputDate = DateTime.Parse(datetime);

    CookieOptions options = new CookieOptions();

    options.Expires = inputDate;
    context.Response.Cookies.Append("StoredValue", value, options);

    context.Response.Redirect("/");
});


app.Run();
